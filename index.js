/*
Write a program that uses fs, os, and dotenv packages 
to read os information of a system and write that in a 
file. The file and directory path should be relative, 
stored as a env variable in .env file. e.g PATH= /Desktop/OSInfo
use promises and async/await functions to perform this task
 */
require("dotenv").config();
const fs = require("fs");
const path = require("path");
const os = require("os");
/* 
console.log(`Your file name is ${process.env.FILE_NAME}`); 
console.log(os.type()); 
console.log(os.release()); 
console.log(os.platform()); 
*/
const osinfo = () => {
  return {
    Type: os.type(),
    Release: os.release(),
    Platform: os.platform(),
    Architecture: os.arch(),
    CPU: os.cpus(),
    "Home direcoty": os.homedir(),
    Hostname: os.hostname(),
  };
};
const getOsInfo = () => {
  return new Promise((resolve, reject) => {
    resolve(osinfo());
  });
};
const writeInfo = (filePath, osInformation) => {
  return new Promise((resolve, reject) => {
    if (fs.existsSync(os.homedir())) {
      //file exists
      resolve(
        fs.writeFile(filePath, JSON.stringify(osInformation), function (err) {
          if (err) {
            console.log(err);
          }
        })
      );
    } else reject(`Something went wrong`);
  });
};

//---write file using async /await starts---//
const saveOsInfo = async () => {
  let osInformation = await getOsInfo();
  // for current project
  //let filePath = path.join(process.env.DIR_PATH + "/" + process.env.FILE_NAME);
  //for home dir
  let filePath = path.join(
    os.homedir() + "/" + process.env.DIR_PATH + "/" + process.env.FILE_NAME
  );
  try {
    //write os information to file.
    await writeInfo(filePath, osInformation);
    console.log("Sucess!");
  } catch (err) {
    // catches errors
    console.log(err);
  }
};

saveOsInfo();
//---write file using async / await ends---//
